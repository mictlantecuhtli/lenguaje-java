/*
Fecha: 24_11_20
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Escriba un algoritmo que, dado el número de horas trabajadas por un empleado 
y el sueldo por hora, calcule el sueldo total de ese empleado.*/

import java.util.Scanner;

public class raul_8 {//Inicio clase principal
	public static void main (String[] args){//Inicio metodo principal
		//Declaracion de variables
		String nombre;
		int hTrabajadas;
		double precioHora, sueldoTotal;
		Scanner teclado = new Scanner(System.in);

		System.out.print("Ingrese el nombre del empleado: ");
		nombre = teclado.nextInt();
		System.out.print("Ingrese las horas trabajadas: ");
		hTrabajadas = teclado.nextInt();
		System.out.print("Ingrese el precio por hora: ");
		precioHora = teclado.nextInt();
		
		sueldoTotal = precioHora*hTrabajadas;

		System.out.print("El sueldo de "+ nombre + "es de: "+ sueldoTotal +"pesos");
	}//Fin metodo principal
}//Fin clase principal
