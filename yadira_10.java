/*
Fecha: 09_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Calcule la sumatoria de N numeros*/

import java.util.Scanner;

public class yadira_10 {//Inicio clase principal
	public static void main(String[] args){//Inicio clase principal
			//Declaracion de variables
			int numeros [] = new int [100];
			int cantidad_Numeros;
			int sumatoria_Numeros = 0;
			Scanner teclado = new Scanner(System.in);

			System.out.print("¿Cuantos numeros desea sumar?: ");
			cantidad_Numeros = teclado.nextInt();

			for ( int i = 1; i <= cantidad_Numeros; i++){//Inicio for_1
				System.out.print("Digite el numero "+ i + ": ");
				numeros[i] = teclado.nextInt();

				sumatoria_Numeros += numeros[i];
			}//Fin for_1
			System.out.print("La sumatoria de todos los números es: "+ sumatoria_Numeros + ": ");
	}//Fin método principal
}//Fin clase principal
