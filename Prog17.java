/*Fecha: 17-08-2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Realice un algoritmo que, con base en una calificación proporciona­da (0-10), indique con letra la calificación que le corresponde: 10 es “A”, 9 es “B”, 8 es “C”, 7 y 6 son “D”, y de 5 a 0 son “F”. Represente el diagrama de flujo, el pseudocódigo correspondiente.*/

import java.util.Scanner;

public class Prog17 {/*Inicio clase*/
	public static void main(String[] args){/*Inicio de metodo principal*/
		/*Declaracion de variables*/
		float costoBoleto;
		float precioKilometro;
		int kilometrosRecorridos;
		Scanner teclado = new Scanner (System.in);
		System.out.print("¿Cual es el costo del kilometro?: ");
		precioKilometro = teclado.nextInt();
		System.out.print("¿Cuantos kilometros va a recorrer?: ");
		kilometrosRecorridos = teclado.nextInt();

		costoBoleto=(precioKilometro*kilometrosRecorridos);
		System.out.print("El precio del boleto es de: " + costoBoleto + " pesos");
	}/*Fin metodo principal*/
}/*Fin de la clase*/
