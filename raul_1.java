/*Una persona recibe un préstamo de $10,000.00 de un
banco y desea saber cuánto pagará de interés, si el 
banco le cobfgra una tasa del 27% anual.*/

//[https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/](https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/)
//[https://www.kumari.net/index.php/system-adminstration/49-mounting-a-qemu-image](https://www.kumari.net/index.php/system-adminstration/49-mounting-a-qemu-image)

import java.util.Scanner;
public class raul_1{/*Inicio clase*/
        public static void main(String[] args) {/*Inicio metodo principal*/
                /*Declaración de variables*/
                int tiempo;
                double PRESTAMO=10000;
                int transcurrido;
                double INTERES=0.27;
                int i;
                Scanner teclado = new Scanner (System.in);

                System.out.print("Inserte el año en que solicito el prestamo: ");
                tiempo = teclado.nextInt();
                System.out.print("Inserte el año actual: ");
                transcurrido = teclado.nextInt();

                for(i=tiempo;i<=transcurrido;i++){/*Inicio for_1*/
                        PRESTAMO=PRESTAMO+(PRESTAMO*INTERES);

                        System.out.println("El interes de cada año "+i+" es de: $"+PRESTAMO+" pesos");
                }/*Fin for_1*/

        }/*Fin metodo principal*/
}/*Fin clase*/
