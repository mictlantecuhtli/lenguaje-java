/*
Fecha: 08_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Genere el factorial de un numero*/

import java.util.Scanner;

public class yadira_9 {//Inicio clase principal
	public static void main(String[] args){//Inicio metodo principal
		//Declaracion de variables
		int numero;
		int numero_Factorial = 1;
		Scanner teclado = new Scanner (System.in);

		System.out.print("Digite un numero: ");
		numero = teclado.nextInt();

		for (int i = 1; i<= numero; i++){//Inicio método principal
			numero_Factorial = numero_Factorial * i;
		}//Fin metodo principal
		System.out.print("El factorial de "+ numero + " es: "+ numero_Factorial);
	}//Fin metodo principal
}//Fin clase principal
