/*
Fecha: 10_02_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail
*/

/*
Tema Estructuras cíclicas 
1_EN EL TECNOLÓGICO DE JILOTEPEC SE REALIZARÁ UNA ENCUESTA, PARA CONOCER 
MAYOR INFORMACIÓN DE LOS ALUMNOS Y VERIFICAR QUIENES DE ELLOS TIENEN O 
HAN TENIDO ALGUNA DIFICULTAD DE CONECTIVIDAD.  PARA LO CUAL LES ESTAN 
HACIENDO UN CUESTIONARIO CON LAS SIGUIENTES PREGUNTAS:
 • EDAD
 • LOCALIDAD
 • TIENEN INTERNET EN CASA
 • TIENEN COMPUTADORA EN CASA
 • CUENTA CON LOS RECURSOS BÁSICOS NECESARIOS DE ALIMENTACION.
2_RECOLECTA LOS DATOS Y PRESENTA LOS RESULTADOS CONSIDERANDO QUE SE 
DESCONOCE LA CANTIDAD DE ALUMNOS. 
3_IMPRIME LOS RESULTADOS CON NÚMEROS Y PORCENTAJES.
*/

import java.util.Scanner;

public class yadira_13 {//Inicio método principal
	public static void main(String[] args){//Inicio metodo principal
		//Declaracion de variables
		String name;
		String materias [] = new String [3];
		int calificacion_materias[][] = new int [3][5];
		String apellido_Paterno;
		String apellido_Materno;
		int grupo;
		String carrera;
		double suma_Promedios = 0;
		double promedio_General;
		double suma_Unidades;
		double suma_promedio_materia = 0;
		int promedio_materia;
		Scanner teclado = new Scanner (System.in);

		System.out.print("Digite su nombre: ");
		name = teclado.nextLine();
		System.out.print("Digite su apellido paterno: ");
		apellido_Paterno = teclado.nextLine();
		System.out.print("Digite su apellido materno: ");
		apellido_Materno = teclado.nextLine();
		System.out.print("Digite su grupo: ");
		grupo = teclado.nextInt();
		System.out.print("Digite su carrera: ");
		carrera = teclado.nextLine();

		for ( int i = 0; i < 3; i++){//Inicio for_1
			System.out.print("\n\nDigite el nombre de la materia " + i+1 +":");
			materias[i] = teclado.nextLine();
			
			for ( int j = 0; j < 5; j++){//Inicio for_2
				System.out.print("Digite el promedio que obtuvo en la unidad "+ j+1 +": ");
				calificacion_materias[i][j] = teclado.nextInt();
			}//Fin for_2
		}//Fin for_1
		for ( int a = 0; a < 3; a++){//Inicio for_3
			promedio_materia = 0;
			suma_Unidades = 0;
			System.out.print("Alumno: "+ name + apellido_Paterno + apellido_Materno);
			System.out.print("\nGrupo: "+ grupo);
			System.out.print("\nCarrera: "+ carrera);
			System.out.print("\nMateria: "+ materias[a]);
			System.out.print("\nU1\tU2\tU3\tU4\tU5\n");

			for ( int b = 0; b < 5; b++){//Inicio for_4
				System.out.print(calificacion_materias[a][b] + "\t");
				promedio_materia = promedio_materia + calificacion_materias[a][b];
			}//Fin for_4
			suma_Unidades = promedio_materia / 5;
			suma_promedio_materia= suma_promedio_materia + suma_Unidades;
			System.out.print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++-");
			System.out.print("Promedio: "+ suma_Unidades);
			System.out.print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++-");
		}//Fin for_3
		promedio_General = suma_promedio_materia / 3;
		System.out.print("El promedio general es: "+ promedio_General);
		System.out.print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++-");
	}//Fin método principal
}//Fin método principal
