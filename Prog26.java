/*
 *Fecha: 29_09_2020
 *Autor:Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com */

/*26-El banco “Bandido de peluche” desea calcular para cada uno de
sus N clientes su saldo actual, su pago mínimo y su pago para no
generar intereses. Además, quiere calcular el monto de lo que ganó 
por concepto interés con los clientes morosos. Los datos que se conocen 
de cada cliente son: saldo anterior, monto de las compras que realizó y
pago que depositó en el corte anterior. Para calcular el pago mínimo se
considera 15% del saldo actual, y el pago para no generar intere­ses 
corresponde a 85% del saldo actual, considerando que el saldo actual 
debe incluir 12% de los intereses causados por no realizar el pago mínimo
y $200 de multa por el mismo motivo. Realice el algo­ritmo correspondiente
y represéntelo mediante diagrama de flujo y pseudocódigo.*/

import java.util.Scanner;
 public class Prog26 {/*Inicio clase*/
	 public static void main(String[] args){/*Inicio metodo principal*/
		 /*Declaracion de variables*/
		 double saldoAnterior = 0;
		 double pagoActual = 0;
		 double saldoActual = 0;
		 double saldoMinimo = 0;
		 double pagoInteres = 0;
		 double montoCompras = 0;
		 double depositoAnterior = 0;
		 int numClientes;
		 int i;
		 String nombre;
		
		Scanner teclado = new Scanner (System.in);

		System.out.print("Ingrese el numero de clientes: ");
		numClientes = teclado.nextInt();
		
		for(i = 0; i < numClientes; i++){/*Inicio for_1*/
		    System.out.print("Ingrese el nombre del cliente "+i+": ");
		    nombre = teclado.next();
		    System.out.print("Ingrese el saldo anterior: ");
		    saldoAnterior = teclado.nextInt();
		    System.out.print("Ingrese su ultimo deposito: ");
		    depositoAnterior = teclado.nextInt();
		    System.out.print("Ingrese el monto por sus ventas realizadas: ");
		    montoCompras = teclado.nextInt();
		    System.out.print("Ingrese el saldo actual: ");
		    saldoActual = teclado.nextInt();

		      pagoActual = (saldoActual*0.12)+200;
		      saldoMinimo = (saldoActual*0.15);
		      pagoInteres = (saldoActual*0.85);

		      System.out.println("El saldo actual de "+ nombre +" es de: $"+ pagoActual +" pesos");
		      System.out.println("El pago minimo de "+ nombre +" es de: $"+ saldoMinimo +" pesos");
                      System.out.println("El pago para no generar intereses de "+ nombre +" es de: $"+ pagoInteres +" pesos");
		}/*Fin for_1*/
	 }/*Fin metodo principal*/
 }/*Fin clase*/
