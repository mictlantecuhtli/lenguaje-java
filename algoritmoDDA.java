/*
Fecha: 16_06_2021
Autor: Fátima Azucena MC
Correo: fatimaazucenamartínez274@gmail.com
*/

import java.util.Scanner;

public class algoritmoDDA {//Inicio clase principal
        public static void main(String[] args){//Inicio metodo principal
                //Declaración de variables
                int a1;
                int b1;
		int a2;
                int b2;
                Scanner teclado = new Scanner (System.in);

                System.out.print("Digite la coordenada de x1: ");
                a1 = teclado.nextInt();
                System.out.print("Digite la coordenada de y1: ");
                b1 = teclado.nextInt();
                System.out.print("Digite la coordenada de x2: ");
                a2 = teclado.nextInt();
                System.out.print("Digite la coordenada de y2: ");
                b2 = teclado.nextInt();
                
		algoritmoDDA(a1,b1,a2,b2);
	}//Inicio metodo principal
	private static int algoritmoDDA(int x1, int y1, int x2, int y2){//Inicio función algoritmoDDA
		int dx;
        	int dy;
        	int steps;
        	int xinc;
        	int yinc;

        	dx = Math.abs(x2-x1);
        	dy = Math.abs(y2-y1);

        	if ( dx > dy){
                	steps = dx;
        	}
        	else {
                	steps = dy;
        	}

        	xinc = dx / steps;
        	yinc = dy / steps;
	
		for (int i = 0; i <= steps; i++){
                	System.out.print("x: " + x1 + " y: " + y1 + "\n");
                	x1 = x1 + xinc;
                	y1 = y1 + yinc;
        	}
		return 0;
	}//Fin función algoritmoDDA
}//Fin clase principal
