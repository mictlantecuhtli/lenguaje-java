/*Fecha: 17-08-2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Escriba un algoritmo que dada la cantidad de monedas de 5-10-20 pesos, 
diga la cantidad de dinero que se tiene en total*/

import java.util.Scanner;

public class raul_7 {/*Inicio clase*/
        public static void main(String[] args){/*Inicio de metodo principal*/
                /*Declaracion de variables*/
                int convertidor=0;
                double resultado=0;
                double  cajero[ ]={ 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, .50, .20, .10};
                Scanner teclado = new Scanner (System.in);
                for(int i=0;i<=12;i++){/*Inicio for 1*/
                        System.out.print("Ingrese la cantidad de $"+ cajero[i] + " pesos que tiene: ");
                        convertidor = teclado.nextInt();
                        resultado=resultado+(convertidor*cajero[i]);
                }/*Fin for 1*/
                System.out.print("El total almacenado es de: "+ resultado +" pesos");
        }/*Fin de metodo principal*/
}/*Fin de la clase*/
