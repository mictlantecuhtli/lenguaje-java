/*
 *Fecha: 03_10_2020
 *Autor: Fatima Azucena Martinez Cadena. 
 *fatimaazucenamartinez274@gmail.com*/

/*Calcula la cantidad de euros a monedas*/

import java.util.Scanner;
public class raul_4 {/*Inicio clase*/
        public static void main(String[] args){/*Inicio metodo principal*/
                /*Declaracion de variables*/
                double DES = 0.35;
                double precio = 0;
                String nombre;
                Scanner teclado = new Scanner (System.in);

                System.out.print("Inserte el nombre del medicamento: ");
                nombre = teclado.nextLine();
                System.out.print("Inserte el precio del medicamento: ");
                precio = teclado.nextInt();

                precio = precio-(precio* DES);

                System.out.print("El total a pagar por su "+ nombre +" es de: $"+ precio +" pesos");
        }/*Fin metodo principal*/
}/*Fin clase*/
