/*
Fecha: 06_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/**Genera un programa que determine si eres de mayor de edad*/

//Paqueteria de java
import java.util.Scanner;

public class yadira_1 {//Inicio clase principal
	public static void main(String[] args){//Inicio metodo principal
		//Declaracion de variables
		int edad;
		Scanner teclado = new Scanner(System.in);

		System.out.print("Digite su edad: ");
		edad = teclado.nextInt();

			if ( edad >= 18){
				System.out.print("Usted es mayor de edad :D");
			}
			else {
				System.out.print("Usted no es mayor de edad :(");
			}
	}//Fin metodo principal
}//Fin clase principal
