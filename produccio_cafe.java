/*
Fecha: 11_06_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

import java.util.Scanner;

public class produccio_cafe {//Inicio clase principal
	public static void main(){//Inicio metodo principal
		//Declaracion de variables
		String [] nombres= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		int i;
		int y;
		int posicion;
		double total_produccion;
		double [] suma_produccion = new double[100];
		Scanner teclado = new Scanner(System.in);

		System.out.print("Digite la producción de cáfe en KG por meses: ");
		for (i = 0; i < 12; i++){
			System.out.print("-->" + nombres[i] +": ");
			suma_produccion[i] = teclado.nextInt();
			total_produccion = total_produccion + suma_produccion[i];
		}//Fin for_1
		total_produccion = total_produccion / 12;
		System.out.print("Producción anual: " + total_produccion);

		System.out.print("Producción mayor al promedio: ");
		for ( int k = 0; k < 12; k++){//Inicio for_2
			if ( suma_produccion[k] < total_produccion){//Inicio condicional_1
				System.out.print("-->" + suma_produccion[k] + "\n");
			}//Fin condicional_1
		}//Fin for_2	

		System.out.print("Producción menor al promedio: ");
                 for ( int p = 0; p < 12; p++){//Inicio for_2
                         if ( suma_produccion[p] < total_produccion){//Inicio condicional_1
                                 System.out.print("-->" + suma_produccion[p] + "\n");
                         }//Fin condicional_1
               }//Fin for_2

	       for (int x = 0; x < 12; x++ ){//Inicio for_3
	       		for (y = 0; y < 11; y++){///Inicio for_4
				if (suma_produccion[y] > suma_produccion[y+1]){//Inicio condicional_1
					posicion = suma_produccion[y + 1];
					suma_produccion[y+1] = suma_produccion[y];
					suma_produccion[y] = posicion;
				}//Fin condicional_1
			}//Fin for_4
	       }//Fin for_3

	       System.out.print("La mayor producción de cáfe es de: " + suma_produccion[11]+ " Kg");
	       System.out.print("La menor producción de cáde es de: " + suma_produccion[0] + " Kg");
	}//Fin metodo principal
}//Fin clase principal


