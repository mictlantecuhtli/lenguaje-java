/*
Fecha: 11_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Genere el promedio de N edades*/

import java.util.Scanner;

public class yadira_8 {//Inicio clase principal
	public static void main(String[] args){//Inicio método principal
		//Declaracion de variables
		int numero_Alumnos;
		int suma_Edades = 0;
		int edad [] = new int [100];
		Scanner teclado = new Scanner (System.in);

		System.out.print("¿Cuantos alumnos son?: ");
		numero_Alumnos = teclado.nextInt();

		for (int i = 1; i <= numero_Alumnos; i++){//Inicio for_1
			System.out.print("Digite la edad del alumno "+ i +":");
			edad[i]= teclado.nextInt();
			suma_Edades += edad[i];
		}//Fin for_1
		suma_Edades = suma_Edades/numero_Alumnos;
		System.out.print("El porcentaje de las edades es: "+ suma_Edades +"\n");
	}//Fin método principal
}//Fin clase principal
