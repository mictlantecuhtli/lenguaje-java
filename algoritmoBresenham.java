/*
Fecha: 16_06_2021
Autor: Fátima Azucena MC
Correo: fatimaazucenamartínez274@gmail.com
*/

import java.util.Scanner;

public class algoritmoBresenham {//Inicio clase principal
	//int algoritmoBresenham (int x1, int y1, int x2, int y2);//Fin función algoritmoBresenham
	public static void main(String[] args){//Inicio metodo principal
		//Declaración de variables
		int a1;
		int b1;
		int a2;
		int b2;
		Scanner teclado = new Scanner (System.in);

		System.out.print("Digite la coordenada de x1: ");
		a1 = teclado.nextInt();
		System.out.print("Digite la coordenada de y1: ");
		b1 = teclado.nextInt();
		System.out.print("Digite la coordenada de x2: ");
                a2 = teclado.nextInt();
                System.out.print("Digite la coordenada de y2: ");
                b2 = teclado.nextInt();

		algoritmoBresenham(a1,b1,a2,b2);
	}//Fin metodo principal
	private static int algoritmoBresenham(int x1, int y1, int x2, int y2){//Inicio función algoritmoBresenham
		int x, y, dx, dy, p;
        	x = x1;
        	y = y1;
        	dx = x2 - x1;
        	dy = y2 - y1;
        	p = 2*dy - dx;

        	while (x <= x2){
                	System.out.print("x: " +  x + " y: " + y + "\n");
                	x++;
              			if ( p < 0){
                        		p = p + 2 * dy;
               			}
                		else{
                        		p = p + ( 2 * dy) - (2 * dx);
					y++;
               			}
        	}//Fin while_1;
		return 0;
	}//Fin función algoritmoBresenham
}//Fin clase principal
