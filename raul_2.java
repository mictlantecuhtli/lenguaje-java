/*Fecha: 02_10_20
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Calcula el precio de un boleto de viaje, tomando en cuenta 
el número de kilómetros que se van a recorrer. El precio por 
Kilometro es de $20.50*/

import java.util.Scanner;

public class raul_2 {/*Inicio clase*/
        public static void main(String[] args){/*Inicio metodo principal*/
                /*Declaracion de variables*/
                double costoBoleto;
                int kilometrosRecorridos;
                double PRECIOKILOMETRO=20.50;
                Scanner teclado = new Scanner (System.in);

                System.out.print("Ingrese los kilometros que va a recorrer: ");
                kilometrosRecorridos = teclado.nextInt();

                costoBoleto = (kilometrosRecorridos*PRECIOKILOMETRO);

                System.out.println("Usted a recorrido "+kilometrosRecorridos+" kilometors y el costo de su boleto es de: $"+costoBoleto+" pesos");
        }/*Fin metodo principal*/
}/*Fin clase*/
