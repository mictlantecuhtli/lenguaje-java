/*
 *Fecha: 20-08-2020
 *Autor: Fatima Azucena MC 
 *fatimaazucenamartinez274@gmail.com*/

/*Se requiere determinar el costo que tendrá realizar una llamada te­lefónica con base en el tiempo que dura la llamada y en el costo por minuto.*/

import java.util.Scanner;

public class Prog22 {/*Inicio de clase*/
	public static void main(String[] args) {/*Inicio metodo principal*/
		/*Declaracion de variables*/
		int minuto;
		int precioMinuto;
		int resultado;
		Scanner teclado = new Scanner (System.in);

		System.out.print("Ingrese el precio por minuto: ");
		precioMinuto = teclado.nextInt();
		System.out.print("Ingrese los minutos que tomo su llamada: ");
		minuto = teclado.nextInt();

		resultado=(precioMinuto*minuto);

		System.out.println("El precio que tomara su llamada es de: $"+ resultado +" pesos");
	}/*Fin metodo principal*/
}/*Fin de clase*/
