/*
Fecha: 11_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Imprime la tabla de multiplicar de un numero N*/

import java.util.Scanner;

public class yadira_11 {//Inicio clase principal
        public static void main(String[] args){//Inicio clase principal
                //Declaración de variables
                int numero;
                int tabla;
                int resultado;
                Scanner teclado = new Scanner (System.in);

                System.out.print("¿Qué tabla desea ver?: ");
                tabla = teclado.nextInt();
                System.out.print("¿Hasta que numero desea ver?: ");
                numero  = teclado.nextInt();

                for(int i = 0; i <= numero; i++){//Inicio for_1
                        resultado = tabla * i;
                        System.out.print(tabla + " x " + i + " = " + resultado);
                        System.out.print("\n");
                }//Fin for_1
        }//Fin método principal
}//Fin clase principal

