/*
Fecha: 05_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmial.com
*/

/*En un hospital existen 3 áreas: Urgencias, Pediatría y Traumatología. 
El presupuesto anual del hospital se reparte de la siguiente manera: 
Pediatría 42% y Traumatología 21%*/

import java.util.Scanner;
public class raul_6 {//Inicio clase principal
	public static void main(String[] args){//Inicio metodo principal
		//Declaracion de variables
       		 int presupuesto_Anual;
       		 double presupuesto_Traumatologia = 0;
        	 double presupuesto_Pedriatia = 0;
        	 double presupuesto_Urgencias = 0;
		 Scanner teclado = new Scanner (System.in);

		 System.out.print("Ingrese el presupuesto anual: ");
		 presupesto_Anual = teclado.nextInt();
		
		 presupuesto_Traumatologia = presupuesto_Anual*0.21;
		 presupuesto_Pediatria = presupuesto_Anual*0.42;
		 presupuesto_Urgencias = presupuesto_Anual*0.37;

		 System.out.print("El presupesto para el área de traumatología es de: $" + presupuesto_Traumatologia + " pesos");
		 System.out.println("El presupesto para el área de pediatría es de: $" + presupuesto_Pediatria + " pesos");
		 System.out.println("El presupesto para el área de urgencias es de: $" + presupuesto_Urgencias + " pesos");
	}//Fin metodo principal
}//Fin clase principal
