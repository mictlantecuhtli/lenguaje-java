/*
Fecha: 28_12_2020
Autor: Fatima Azucena Martínez Cadena
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Obtener la edad de una persona en meses, si se ingresa su edad
en años y meses. Ejemplo: Ingresado 3 años 4 meses debe
mostrar 40 meses.*/

import java.util.Scanner;

public class raul_11 {//Inicio clase principal
	public static void main(String[] args){//Inicio metodo principal
		//Declaracion de varibles
		int anyos;
		int meses;
		int edad = 0;
		Scanner teclado = new Scanner (System.in);

		System.out.print("Ingrese su edad en años: ");
		anyos = teclado.nextInt();
		System.out.print("Con cuantos meses: ");
		meses = teclado.nextInt();

		edad = edad + (anyos*12) + meses;

		System.out.println("Su edad en meses es de: " + edad);
	}//Fin metodo principal
}//Fin clase principal
