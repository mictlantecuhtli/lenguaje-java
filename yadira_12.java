/*
Fecha: 20_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*
Tema Estructuras cíclicas 
1_EN EL TECNOLÓGICO DE JILOTEPEC SE REALIZARÁ UNA ENCUESTA, PARA CONOCER 
MAYOR INFORMACIÓN DE LOS ALUMNOS Y VERIFICAR QUIENES DE ELLOS TIENEN O 
HAN TENIDO ALGUNA DIFICULTAD DE CONECTIVIDAD.  PARA LO CUAL LES ESTAN 
HACIENDO UN CUESTIONARIO CON LAS SIGUIENTES PREGUNTAS:
 • EDAD
 • LOCALIDAD
 • TIENEN INTERNET EN CASA
 • TIENEN COMPUTADORA EN CASA
 • CUENTA CON LOS RECURSOS BÁSICOS NECESARIOS DE ALIMENTACION.
2_RECOLECTA LOS DATOS Y PRESENTA LOS RESULTADOS CONSIDERANDO QUE SE 
DESCONOCE LA CANTIDAD DE ALUMNOS. 
3_IMPRIME LOS RESULTADOS CON NÚMEROS Y PORCENTAJES.
*/

import java.util.Scanner;

public class yadira_12 {//Inicio clase principal
	public static void main(String[] args){//Inicio método principal
		//Declaracion de variables
		int localidad[] = new int [100];
		int edad[] = new int[100];
		int internet;
        	int alimentacion;
        	int computadora;
        	double internet_si;
        	double internet_no;
        	double computadora_si;
        	double computadora_no;
        	double alimentacion_buena;
        	double alimentacion_mala;
        	double porcentaje;
        	int conInternet = 0;
        	int sinInternet = 0;
        	int conComputadora = 0;
        	int sinComputadora = 0;
        	int buena_Alimentacion = 0;
        	int mala_Alimentacion = 0;
        	int cantidad_Alumnos;
        	int canalejas = 0;
        	int comunidad = 0;
        	int san_Juan = 0;
        	int continuar = 0;
        	int i;
		Scanner teclado = new Scanner (System.in);
		
		do{//Inicio do-while	
			System.out.print("Digite el numero de alumnos: ");
			cantidad_Alumnos = teclado.nextInt();	
			for ( i = 1; i <= cantidad_Alumnos; i++){//Inicio for_1
					System.out.print("\nDigite la edad del alumno "+ i +": ");
					edad[i] = teclado.nextInt();
					System.out.print("\n1)Canalejas\n2)San Juan Acazuchitlan\n3)Comunidad");
					System.out.print("\nDigite su localidad: ");
					localidad[i] = teclado.nextInt();

				switch (localidad[i]){//Inicio switch-case
					case 1:
                                        	canalejas += 1;
                                	break;
                                	case 2:
                                        	san_Juan += 1;
                                	break;
                                	case 3:
                                        	comunidad += 1;
                                	break;
					default:
						System.out.print("Opcion invalida, intente de nuevo");
					break;
				}//Fin switch-case

				System.out.print("\n¿Cuenta con internet en casa?\n1)Si\n0)No");
				System.out.print("\nDigite su opcion: ");
				internet = teclado.nextInt();

				if ( internet == 1){//Inicio if_1
                                	conInternet += 1;
                        	}//Fin if_1
                                	else if ( internet == 0){//Inicio if_else_1
                                        	sinInternet += 1;
                                	}//Fin if_else_1

				System.out.print("\n¿Cuenta con computadora?:\n1)Si\n0)No");
				System.out.print("\nDigite su opcion: ");
				computadora = teclado.nextInt();

				if ( computadora == 1){//Inicio if_2
                                	conComputadora += 1;
                        	}//Fin if_2
                                	else if ( computadora == 0){//Inicio if_else_2
                                        	sinComputadora += 1;
                                	}//Fin if-else_2
		
				System.out.print("\n¿Cuenta con una buena alimentacion?:\n1)Si\n0)No");
				System.out.print("\nDigite su opcion: ");
				alimentacion = teclado.nextInt();

				if ( alimentacion == 1){//Inicio if_3
                                	buena_Alimentacion += 1;
                        	}//Fin if_3

                                	else if ( alimentacion == 0){//Inicio if_else_3
                                        	mala_Alimentacion += 1;
                                	}//Fin if_else_3
			}//Fin for_1
				porcentaje = (100/cantidad_Alumnos);
                                internet_si = porcentaje*conInternet;
                                internet_no = porcentaje*sinInternet;
                                computadora_si = porcentaje*conComputadora;
                                computadora_no = porcentaje*sinComputadora;
                                alimentacion_mala = porcentaje*mala_Alimentacion;
                                alimentacion_buena = porcentaje*buena_Alimentacion;

                                System.out.print("\nSe realizo la encuesta a "+ cantidad_Alumnos +" alumnos");
                                System.out.print("\nDe esos alumnos "+ conInternet +" dijeron que contaban con internet, lo que corresponde a un: ");
                                System.out.print("\n"+ internet_si +" porciento");
                                System.out.print("\n"+ sinInternet +" alumnos dijeron que no contaban con internet, lo que corresponde a un: ");
                                System.out.print("\n"+ internet_si +" porciento");
                                System.out.print("\n"+ conComputadora +" alumnos dijeron que contaban con computadora, lo que correponde a un: ");
                                System.out.print("\n"+ computadora_si +" porciento");
                                System.out.print("\n"+ sinComputadora +" alumnos dijeron que no contaban con una computadora, lo que corresponde a:");
                                System.out.print("\n"+ computadora_no +" porciento");
                                System.out.print("\n"+ buena_Alimentacion +" alumnos dijeron que tenian una buena alimentacion, lo que corresponde ");
                                System.out.print("a un: \n"+ alimentacion_buena +" porciento");
                                System.out.print("\n"+ mala_Alimentacion +" alumnos dijeron que tenian una mala alimentacion, lo que corresponde ");
                                System.out.print("a un: \n"+ alimentacion_mala +" porciento");
                                System.out.print("\n\n¿Que desea hacer?:\n1)Volver al ménu\n0)Salir");
                                System.out.print("\nDigite su opcion: ");
                                continuar = teclado.nextInt();

		}while ( continuar == 1);
	}//Fin método principal
}//Fin clase principal
